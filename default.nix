{ system ? builtins.currentSystem }:

let
  modList = import ./lib/modules.nix;
in rec {
  sources = import ./nix/sources.nix;

  # composable things
  modules = modList { modulesDir = ./modules; };
  profiles = modList { modulesDir = ./profiles; };
  overlays = modList { modulesDir = ./overlays; importAll = true; };
  users = modList { modulesDir = ./users; };

  pkgs = import ./pkgs { overlays = builtins.attrValues overlays; };
  inherit (pkgs.nixpkgs) lib;
  inherit (import ./lib/hosts.nix { inherit system modules profiles overlays users sources; pkgs = pkgs; }) hosts groups;
  deploy = import ./lib/deploy.nix { pkgs = pkgs.nixpkgs; inherit hosts groups; };
}
