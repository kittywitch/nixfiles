{ config, lib, pkgs, modules, ... }:

{
  imports = [
    modules.dendrite
  ];
  networking.firewall.allowedTCPPorts = [ 8448 ];
  fileSystems."/var/lib/dendrite" = {
    device = "/persist/var/lib/dendrite";
    options = [ "bind" ];
  };
  hexchen.dendrite = {
    enable = true;
    config = {
      global = {
        server_name = "chaoswit.ch";
        privateKey = "${config.hexchen.dendrite.dataDir}/matrix_key.pem";
        kafka.addresses = [];
      };
      metrics.enabled = true;
      client_api = {
        registration_disabled = true;
      };
      federation_api = {
        federation_certificates = [
          "${config.security.acme.certs."dendrite.chaoswit.ch".directory}/cert.pem"
        ];
      };
      media_api = {
        dynamic_thumbnails = true;
      };
      logging = [
        {
          type = "file";
          level = "warn";
          params.path = "${config.hexchen.dendrite.dataDir}/log";
        }
      ];
    };
  };

  hexchen.dns.zones."chaoswit.ch" = { inherit (config.hexchen.dns.zones."chaoswit.ch".subdomains."${config.networking.hostName}") A AAAA; };
  hexchen.dns.zones."chaoswit.ch".subdomains."dendrite".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.virtualHosts = {
    "chaoswit.ch" = {
      forceSSL = true;
      enableACME = true;
      locations = {
        "/.well-known/matrix/server" = {
          extraConfig = ''
            add_header Content-Type application/json;
          '';
          return = ''200 '{ "m.server": "dendrite.chaoswit.ch:443" }' '';
        };
        "/.well-known/matrix/client" = {
          extraConfig = ''
            add_header Content-Type application/json;
            add_header Access-Control-Allow-Origin *;
          '';
          return = ''200 '{ "m.homeserver": {"base_url": "https://dendrite.chaoswit.ch"}, "m.identity_server":  { "base_url": "https://vector.im"} }' '';
        };
      };
    };
    "dendrite.chaoswit.ch" = {
      forceSSL = true;
      enableACME = true;
      listen = [
        { addr = "0.0.0.0"; port = 443; ssl = true; }
        { addr = "[::]"; port = 443; ssl = true; }
        { addr = "0.0.0.0"; port = 80; ssl = false; }
        { addr = "[::]"; port = 80; ssl = false; }
        { addr = "0.0.0.0"; port = 8448; ssl = true; }
        { addr = "[::]"; port = 8448; ssl = true; }
      ];
      locations = {
        "/_matrix" = {
          proxyPass = "http://localhost:8008";
        };
        "/" = {
          root = (pkgs.element-web.override {
            conf = {
              default_server_name = "chaoswit.ch";
              default_server_config = "";
            };
          }).outPath;
        };
      };
    };
  };
}
