{ config, lib, pkgs, evalConfig, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."pad".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  containers.codimd = {
    privateNetwork = true;
    hostAddress = "192.168.100.1";
    localAddress = "192.168.100.3";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/container/codimd";
        isReadOnly = false;
      };
    };
    path = (evalConfig {hosts = {}; groups = {};} ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";

      imports = [
        profiles.nopersist
      ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.hedgedoc = {
        enable = true;
        configuration = {
          allowAnonymous = false;
          allowAnonymousEdits = true;
          allowFreeURL = true;
          allowGravatar = false;
          allowOrigin = [ "localhost" "pad.chaoswit.ch" ];
          dbURL = "postgres://codimd:codimd@localhost:5432/codimd";
          defaultPermission = "private";
          domain  = "pad.chaoswit.ch";
          host = "0.0.0.0";
          protocolUseSSL = true;
          hsts.preload = false;
          allowEmailRegister = false;
        };
      };
      services.postgresql = {
        enable = true;
        ensureDatabases = [ "codimd" ];
        ensureUsers = [{
          name = "codimd";
          ensurePermissions = {
            "DATABASE codimd" = "ALL PRIVILEGES";
          };
        }];
      };
    })).config.system.build.toplevel;
  };

  services.nginx.virtualHosts."pad.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://192.168.100.3:3000";
      extraConfig = ''
        proxy_pass_request_headers on;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;
        proxy_buffering off;
      '';
    };
  };
}
