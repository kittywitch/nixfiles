{ config, lib, pkgs, sources, ... }:

let
  defaultDns = with pkgs.dns.combinators; {
    MX = [ (mx.mx 20 "koryaksky.chaoswit.ch.") (mx.mx 50 "cornbox.chaoswit.ch.") ];
    TXT = [ (spf.strict  [ "+mx" ]) ];
    subdomains."_dmarc".TXT = [ "v=DMARC1; p=none; ruf=postmaster@hxchn.de" ];
  };
in {
  imports = [
    sources.nixos-mailserver.outPath
  ];

  hexchen.dns.zones."chaoswit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [ "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQWya63yD/3yMGxjrPF2MVxQZIMVKeTaZR9sh9S29aSb4vPUF0R09Xvm3jDHd4eXd3agxhYBMSt/yjHNy8m79x9um2txhYtS1AxVzTUJRdZR+UMd/JCKwqn+t0XM+PrZJGEbMoc7mE9qwGVq7WN0OZiCw5b1wBhEvei40CpS2wbwIDAQAB" ];
  };
  hexchen.dns.zones."lilwit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [ "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJwgkGDkv/o+MdhZmkZbBcXccjm92lDalQvbKzTSDTG5PJjCXmn5QU3gerxV0z7kOaKbGETk+JEyh7VirX/ED1p1puL9Ovy2Wd4V4KPEgBPuUg87Q1PU2+3L0GKeCtwnsb13jaikKZ9EbVj+N5jnE3F+WJ8f9sVprgvSa9Wm8QnwIDAQAB" ];
  };
  hexchen.dns.zones."copyonwit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [ "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGjcWxYP2kMhQmxKpxSNAU2iDlYQlBayL9Vtrh3FIXQBQL425d+yDjsWskey6i1J7r/UAJuHXD5FOmMU6Ee3GXgsvKCaqOfqozv2D6LhSRBTjOqJZ5EaFqpHyNKATMPc+KQdV0isxN5j/lfgIihhV1ySrcItoGwB7Kvh5kUU1htwIDAQAB" ];
  };

  mailserver = {
    mailDirectory = if config.networking.hostName == "cornbox"
                    then "/data/mail"
                    else "/persist/mail";
    enable = true;
    fqdn = "${config.networking.hostName}.${config.networking.domain}";
    domains = [ "hxchn.de" "lilwit.ch" "copyonwit.ch" "chaoswit.ch" ];
    loginAccounts."hexchen@lilwit.ch" = {
      hashedPassword = "$6$LSplQKTGbCa.$dAjTykAUKheeZzB0L7lhPQagnZ7GodJRtt4dBbj4a.f/Cide0cXrmd.WDeFmyFkDYW9vEn/oWk/7ERpTr.dHV/";
      catchAll = [ "hxchn.de" "lilwit.ch" "copyonwit.ch" "chaoswit.ch" ];
    };

    certificateScheme = 3;

    enablePop3 = false;
    enablePop3Ssl = false;
    enableImap = false;
    enableImapSsl = true;
    enableManageSieve = true;

    virusScanning = false;
    localDnsResolver = false;
  };

  services.postfix.submissionsOptions.smtpd_sender_restrictions = "reject_non_fqdn_sender,reject_unknown_sender_domain,permit";

  services.rspamd.extraConfig = ''
    actions {
      reject = null; # Disable rejects, default is 15
      add_header = 4; # Add header when reaching this score
      greylist = 10; # Apply greylisting when reaching this score
    }
  '';
}
