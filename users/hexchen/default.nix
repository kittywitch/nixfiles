rec {
  base = ./base.nix;
  emacs = ./emacs.nix;
  desktop = ./desktop.nix;
  development = ./development.nix;
  sway = ./sway.nix;
  server = { imports = [ base emacs development ]; };
  desktopFull = { imports = [ base emacs desktop development sway ]; };
}
