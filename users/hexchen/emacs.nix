{ config, lib, pkgs, sources, ... }:
let
  emacs = pkgs.callPackage sources.nix-doom-emacs {
    doomPrivateDir = "${./doom.d}";
    emacsPackagesOverlay = self: super: {
      magit-delta = super.magit-delta.overrideAttrs (esuper: {
        buildInputs = esuper.buildInputs ++ [ pkgs.git ];
      });
    };
  };
in {
  users.users.hexchen.packages = [ emacs pkgs.sqlite ];
  home-manager.users.hexchen = {
    home.file.".emacs.d/init.el".text = ''
      (load "default.el")
    '';
  };
}
