{ config, lib, pkgs, ... }:

{
  home-manager.users.hexchen = {
    programs.direnv = {
      enable = true;
      enableFishIntegration = true;
      nix-direnv.enable = true;
    };
    programs.fish = {
      enable = true;
      shellAliases = {
        icat = "${pkgs.kitty}/bin/kitty +kitten icat --align=left";
        fcurl = "curl -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0'";
        fcurlj = "fcurl -H 'Accept: application/json'";
      };
      functions = {
        fish_right_prompt = {
          body = ''
            if test -n "$nshellpkgs"
              echo -n "("(set_color yellow)nix(set_color white):(set_color green)"$nshellpkgs"(set_color white)")"
              set_color white
            else
              echo -n "("(set_color brblue)sys(set_color white)")"
            end
          '';
          description = "Prints the right prompt, containing nix-shell information";
        };
        nex = {
          description = "Execute a command using a nix package";
          argumentNames = [ "pkg" ];
          body = ''
            argparse -i "f/file=?" -- $argv[2..-1]
            if test -z "$file"
              set file "<nixpkgs>"
            end
            nix run -f "$file" "$pkg" $argv
          '';
        };
        nsh = {
          description = "Open a new nix shell with the specified packages";
          body = ''
            argparse -i "f/file=?" -- $argv
            if test -z "$file"
              if test -e default.nix
                set file "."
              else
                set file "<nixpkgs>"
              end
            end
            set -l prevpkgs $nshellpkgs
            set -x nshellpkgs "$nshellpkgs $argv"
            nix run -f "$file" $argv -c fish 
            set -x nshellpkgs $prevpkgs
          '';
        };
        nre = {
          description = "Opens the nix repl";
          body = ''
            if test $argv[1]
              nix repl $argv
            else if test -e default.nix
              nix repl .
            else
              nix repl '<nixpkgs>'
            end
          '';
        };
        nev = {
          description = "Evaluates a nix expression";
          body = ''
            argparse -i "f/file=?" -- $argv
            if test -z "$file"
              if test -e default.nix
                set file "."
              else
                set file "<nixpkgs>"
              end
            end
            nix eval -f $file $argv
          '';
        };
      };
      interactiveShellInit = ''
        ${if (lib.elem "desktop" config.hexchen.deploy.groups)
            then "export SSH_AUTH_SOCK=(gpgconf --list-dirs agent-ssh-socket)"
            else ""}
        set fish_greeting ""
        bass source /etc/profile
      '';
      plugins = [
        {
          name = "bass";
          src = pkgs.fetchFromGitHub {
            owner = "edc";
            repo = "bass";
            rev = "d63054b24c2f63aaa3a08fb9ec9d0da4c70ab922";
            sha256 = "0pwci5xxm8308nrb52s5nyxijk0svar8nqrdfvkk2y34z1cg319b";
          };
        }
      ];
    };
  };
}
