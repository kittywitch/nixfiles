{ config, lib, pkgs, ... }:

{
  users.users.hexchen.packages = with pkgs; [
    crystal shards ameba
    go python38
    elixir lfe rebar3 elixir_ls erlang erlang-ls
    niv nixfmt patchelf
    flashrom ifdtool cbfstool nvramtool
    protobuf
    rustup rust-analyzer
  ];
  home-manager.users.hexchen = {
    services.lorri.enable = true;
  };
}
