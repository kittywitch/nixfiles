{ config ? {}, system ? builtins.currentSystem, ... }@args:

let
  sources = import ../nix/sources.nix;
  pkgs = import sources.nixpkgs args;
  nixpkgs = import sources.nixpkgs (args // { overlays = [];});

in pkgs // { inherit nixpkgs; }
