{ config, lib, pkgs, profiles, ... }:

{
  imports = [
    profiles.base
  ];
  boot.plymouth.enable = true;
  nixpkgs.config.allowUnfree = true;
  powerManagement.cpuFreqGovernor = lib.mkOverride 998 "ondemand";
  programs.light.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.printing = {
    enable = true;
    drivers = with pkgs; [ foomatic-filters foo2zjs ];
    browsing = true;
  };

  networking.useDHCP = true;
  networking.wireless.enable = true;
  hardware.opengl.enable = true;

  services.avahi.enable = true;

  services.udev.packages = [ pkgs.yubikey-personalization pkgs.libu2f-host ];

  services.xserver.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.displayManager.sddm.enable = true;

  boot.supportedFilesystems = [ "zfs" ];

  environment.shellInit = ''
    export GPG_TTY="$(tty)"
    gpg-connect-agent /bye
  '';
}
