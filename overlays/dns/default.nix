self: super: {
  dns = import (super.fetchFromGitHub {
    owner = "kirelagin";
    repo = "nix-dns";
    rev = "985797066ffaf8fb05693169c7c2c3222c704365";
    sha256 = "0mjr6jbcglivr2p47vd53jmp3hygdnk50c5w47sligjkp9wb4ily";
  } + "/dns") { pkgs = self; };
}
