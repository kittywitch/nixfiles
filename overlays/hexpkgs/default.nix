let
  sources = import ../../nix/sources.nix;
in self: super: {
  dino = super.dino.overrideAttrs ({patches ? [], ...}: {
    patches = patches ++ [
      "${sources.qyliss-nixlib}/overlays/patches/dino/0001-add-an-option-to-enable-omemo-by-default-in-new-conv.patch"
    ];
  });

  dendrite = super.callPackage ./dendrite {};

  hh3 = super.callPackage ./hh3 {};
  hh3cord = super.discord.overrideAttrs (dsuper: {
    installPhase = dsuper.installPhase + "\n" + ''
        ${self.hh3}/bin/hh3 $out/opt/Discord/Discord
      '';
  });
  hh3cord-canary = super.discord-canary.overrideAttrs (dsuper: {
    installPhase = dsuper.installPhase + "\n" + ''
        ${self.hh3}/bin/hh3 $out/opt/DiscordCanary/DiscordCanary
      '';
  });

  botamusique = super.callPackage ./botamusique { };
  yggdrasil = super.callPackage ./yggdrasil {};
}
