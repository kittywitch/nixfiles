{ fetchFromGitHub, buildGoModule, lib }:

buildGoModule rec {
  pname = "dendrite";
  version = "0.3.11";

  src = fetchFromGitHub {
    owner = "matrix-org";
    repo = "dendrite";
    rev = "v${version}";
    sha256 = "15xqd4yhsnnpz5n90fbny9i8lp7ki2z3fbpbd8cvsp49347rm483";
  };
  vendorSha256 = "1dr0ma4dgw4ciwbcd4das4hd8yy3g7k55jal237903byf9fgzx9f";
  runVend = true;
  doCheck = true;

  postBuild = ''
    mkdir -p $out/share/dendrite
    cp dendrite-config.yaml $out/share/dendrite/dendrite-config-default.yaml
  '';

  meta = with lib; {
    homepage = https://github.com/matrix-org/dendrite;
    description = "A second-generation Matrix homeserver written in Go";
    license = licenses.asl20;
  };
}
