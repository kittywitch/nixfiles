{ mkYarnPackage }:

mkYarnPackage {
  name = "hh3";
  # currently, hh3 is private. builds will fail unless you have been given access to the repo.
  src = builtins.fetchGit {
    url = "git@gitlab.com:Mstrodl/hh3.git";
    rev = "47f2fe01184f09d813df4f7b92d8e0c47ec6987e";
    ref = "master";
  };

  yarnLock = ./yarn.lock;
}
