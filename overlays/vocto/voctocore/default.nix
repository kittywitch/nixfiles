{
  python3Packages, fetchFromGitHub,
  wrapGAppsHook,
  src
}:

python3Packages.buildPythonApplication rec {
  pname = "voctocore";
  version = "2.0a";
  format = "other";
  inherit src;

  nativeBuildInputs = [ wrapGAppsHook ];

  propagatedBuildInputs = with python3Packages; [
    vocto
    sdnotify
    scipy
  ];

  strictDeps = false;

  postPatch = ''
    substituteInPlace voctocore/voctocore.py --replace "sys.path.insert(0, '.')" "sys.path.insert(0, '$out/lib/voctocore')"
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin $out/lib
    cp -r "voctocore" $out/lib/
    ln -s $out/lib/voctocore/voctocore.py $out/bin/voctocore
    runHook postInstall
  '';

  postFixup = ''
    patchPythonScript $out/lib/voctocore/voctocore.py
  '';
}
