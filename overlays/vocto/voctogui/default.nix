{
  python3Packages, fetchFromGitHub,
  wrapGAppsHook,
  gtk3,
  src
}:

python3Packages.buildPythonApplication rec {
  pname = "voctogui";
  version = "2.0a";
  format = "other";
  inherit src;

  nativeBuildInputs = [ wrapGAppsHook ];

  propagatedBuildInputs = [
    gtk3
  ] ++ (with python3Packages; [
    vocto
    scipy
  ]);

  strictDeps = false;

  postPatch = ''
    substituteInPlace voctogui/voctogui.py --replace "sys.path.insert(0, '.')" "sys.path.insert(0, '$out/lib/voctogui')"
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin $out/lib
    cp -r "voctogui" $out/lib/
    ln -s $out/lib/voctogui/voctogui.py $out/bin/voctogui
    runHook postInstall
  '';

  postFixup = ''
    patchPythonScript $out/lib/voctogui/voctogui.py
  '';
}
