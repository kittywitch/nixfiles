{...}:

{
  home-manager.sharedModules = [
    ({ config, lib, ... }:
    with lib;

    let
      cfg = config.programs.hh3;
    in  {
      options.programs.hh3 = {
        enable = mkEnableOption "hh3 discord thing";
        config = mkOption {
          type = types.attrsOf types.attrs;
          default = {};
        };
        modules = mkOption {
          type = types.listOf types.str;
          default = [ "loadingScreen" "pseudoscience" "sentrynerf" ];
        };
        branch = mkOption {
          type = types.str;
          default = "discord";
        };
      };

      config = mkIf cfg.enable (let
        gen = {
          modules = listToAttrs (map (name: { inherit name; value = { enabled = true; }; }) cfg.modules);
        };
      in {
        home.file.".config/hh3/${cfg.branch}.json".text = builtins.toJSON (recursiveUpdate cfg.config gen);
      });
    })
  ];
}
