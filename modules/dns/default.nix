{ config, lib, pkgs, hosts ? { a = { inherit config;}; }, ... }:

with lib;

let
  cfg = config.hexchen.dns;
  dns = pkgs.dns;
in {
  options.hexchen.dns = with lib; {
    enable = mkEnableOption "hexchens dns stuff";
    primary = mkOption {
      type = types.bool;
      description = "make this server a primary server";
      default = true;
    };
    xfrIPs = mkOption {
      type = types.listOf types.str;
      description = "Own IPs to do AXFR with";
      default = [];
    };
    zones = mkOption {
      type = types.attrsOf dns.types.subzone;
      description = "DNS zones";
      default = {};
    };
    allZones = mkOption {
      type = types.attrsOf dns.types.zone;
      description = "all zones, merged from all hosts (if applicable)";
      default = {};
    };
    dnssec.enable = mkOption {
      type = types.bool;
      default = false;
      description = "enable dnssec signing";
    };
    dnssec.keyDirectory = mkOption {
      type = types.str;
      default = "/etc/hexdns/keys";
      description = "path to the dnssec signing keys";
    };
    dnssec.doSplitSigning = mkOption {
      type = types.bool;
      default = false;
      description = "Do KSK/ZSK split";
    };
    symlinkZones = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      { assertion = !(cfg.dnssec.enable && (cfg.dnssec.keyDirectory == null));
        message = "Please specify hexchen.dns.dnssec.keyDirectory";
      }
    ];

    networking.firewall.allowedUDPPorts = [ 53 ];
    networking.firewall.allowedTCPPorts = [ 53 ];

    hexchen.dns.allZones = mkIf cfg.primary (mkMerge (
      mapAttrsToList (
        name: host: host.config.hexchen.dns.zones
      ) hosts
    ));

    services.nsd = let
      cfgs = filter (c: c.enable) (mapAttrsToList (name: host: host.config.hexchen.dns) hosts);
      primaries = filter (c: c.primary) cfgs;
      secondaries = filter (c: !c.primary) cfgs;
    in {
      enable = true;
      interfaces = [ "::" "0.0.0.0" ];
      port = 53;
      zones = mapAttrs (name: zone:
        if cfg.primary then {
          data = toString zone;
          dnssec = cfg.dnssec.enable;
          dnssecPolicy.algorithm = "ECDSAP256SHA256";
          provideXFR = map (ip: "${ip} NOKEY") (flatten (map (c: c.xfrIPs) secondaries));
          notify = map (ip: "${ip} NOKEY") (flatten (map (c: c.xfrIPs) secondaries));
        } else {
          data = toString zone;
          requestXFR = map (ip: "${ip} NOKEY") (flatten (map (c: c.xfrIPs) primaries));
          allowNotify = map (ip: "${ip} NOKEY") (flatten (map (c: c.xfrIPs) primaries));
        }
      ) cfg.allZones;
    };

    environment.etc = mkIf cfg.symlinkZones (mapAttrs' (name: zone: {
      name = "hexdns/zones/${name}.zone";
      value = { source = pkgs.writeText "${name}.zone" (toString zone); };
    }) cfg.allZones);
  } // {
    nixpkgs.overlays = [ (import ../../overlays/dns) ];
  };
}
