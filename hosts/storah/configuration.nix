{ config, pkgs, lib, profiles, modules, ... }:

let
  addr4 = "195.39.247.68";
  addr6 = "2a0f:4ac0:1337::14";
in {
  imports = [
    profiles.server
    profiles.network
    ./hardware-config.nix
    ./zfs.nix
    ../../services/restic/server.nix
    ../../services/syncthing.nix
    ../../services/zfs.nix
    ../../services/dns
  ];

  hexchen.encboot = {
    enable = true;
    networkDrivers = [ "r8169" ];
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.devices = [ "/dev/sda" "/dev/sdb" "/dev/sdc" "/dev/sdd" ];
  boot.supportedFilesystems = [ "zfs" ];

  networking.hostName = "storah";
  networking.hostId = "7e6af5a5";

  networking.interfaces.enp3s0.ipv6.addresses = [ {
    address = "2a01:4f8:140:21e4::1";
    prefixLength = 64;
  } ];

  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "enp3s0";
  };
  networking.policyrouting.rules4 = lib.singleton { rule = "from 46.4.62.95 lookup main"; prio = 8500; };

  hexchen.dns.zones."chaoswit.ch".subdomains."storah.hetzner".A = [ "46.4.62.95" ];
  hexchen.dns.zones."chaoswit.ch".subdomains.storah = pkgs.dns.combinators.host addr4 addr6;

  hexchen.dns.xfrIPs = [ addr4 ];
  hexchen.dns.primary = false;

  networking.nameservers = [ "1.1.1.1"  "1.0.0.1" ];
  networking.useDHCP = true;

  nixpkgs.config.allowUnfree = true;

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    listen = {
      enable = true;
      endpoints = [ "tcp://46.4.62.95:31079" ];
    };
    pubkey = "cb5a8d206dd9ee0c4134ea6b136d542cdceb313f3c9dfbcbb257b082241cf741";
  };

  network.wireguard = {
    magicNumber = 2;
    pubkey = "Nu5NOV4AIblqHvPzisqvNatXwS8FAJ3ehoYpLLGHLmc=";
    publicAddress4 = "46.4.62.95";
  };
  network.bird = {
    staticRoutes4 = [
      "${addr4}/32 blackhole"
    ];
    staticRoutes6 = [
      "${addr6}/128 blackhole"
    ];
  };

  network.prefsrc4 = addr4;
  network.prefsrc6 = addr6;

  networking.interfaces.lo.ipv4.addresses = lib.singleton { address = addr4; prefixLength = 32; };
  networking.interfaces.lo.ipv6.addresses = lib.singleton { address = addr6; prefixLength = 128; };

  system.stateVersion = "20.09";
}
