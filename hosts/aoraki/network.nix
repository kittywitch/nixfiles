{ config, lib, pkgs, modules, ... }:

{
  imports = [
    modules.nftnat
    modules.network.bird
  ];

  boot.blacklistedKernelModules = [ "ip_tables" "ip6_tables" "x_tables"];

  networking.useDHCP = false;
  networking.interfaces.fritzbox.useDHCP = true;
  networking.interfaces.manglement.useDHCP = true;
  networking.dhcpcd.extraConfig = ''
    noipv6rs
    noipv6
  '';

  networking.interfaces.fritzbox.macAddress = "44:d9:e7:9f:65:9a";
  networking.interfaces.fritzbox.mtu = 1452;

  networking.interfaces.enp3s0.ipv4.addresses = [{ address = "195.39.247.81"; prefixLength = 28; }];
  networking.interfaces.enp3s0.ipv6.addresses = [{ address = "2a0f:4ac0:1337:666::1"; prefixLength = 64; }];
  networking.interfaces.weesix.ipv6.addresses = [{ address = "2a0f:4ac0:1337:661::1"; prefixLength = 64; }];
  networking.interfaces.privnet.ipv4.addresses = [{ address = "192.168.40.1"; prefixLength = 24; }];
  networking.interfaces.privnet.ipv6.addresses = [{ address = "2a0f:4ac0:1337:662::1"; prefixLength = 64; }];

  boot.kernel.sysctl = {
    "net.ipv6.conf.default.accept_ra" = 0;
    "net.ipv6.conf.all.forwarding" = true;
    "net.ipv4.conf.all.forwarding" = true;
  };

  networking.vlans = let
    vlans = {
      manglement = 10;
      upstream = 20;
      ioshit = 30;
      privnet = 40;
      fritzbox = 50;
      # gpon = 51;
      weesix = 60;
    };
  in lib.mapAttrs (_: id: { inherit id; interface = "enp3s0"; }) vlans;

  services.dhcpd4 = {
    enable = true;
    authoritative = true;
    extraConfig = ''
      option domain-name-servers 1.1.1.1, 1.0.0.1;
      subnet 195.39.247.80 netmask 255.255.255.240 {
        option broadcast-address 195.39.247.95;
        option routers 195.39.247.81;
        option subnet-mask 255.255.255.240;
        range 195.39.247.82 195.39.247.94;
      }
      subnet 192.168.40.0 netmask 255.255.255.0 {
        option routers 192.168.40.1;
        option subnet-mask 255.255.255.0;
        option broadcast-address 192.168.40.255;
        range 192.168.40.10 192.168.40.240;
      }
    '';
    interfaces = [ "enp3s0" "privnet" ];
  };

  services.radvd = {
    enable = true;
    config = ''
      interface enp3s0 {
        prefix ::/64 {};
        AdvSendAdvert on;
      };
      interface privnet {
        prefix ::/64 {};
        AdvSendAdvert on;
      };
      interface weesix {
        prefix ::/64 {};
        AdvSendAdvert on;
        RDNSS 2a0f:4ac0:1337:64::1 {};
      };
    '';
  };

  networking.nftables.extraInput = ''
    meta l4proto 89 iifname wgmesh-* accept
  '';

  networking.nftables.extraForward = ''
    udp dport 53 ip daddr 195.39.247.81/28 drop
    meta nfproto ipv6 tcp flags syn tcp option maxseg size 1325-65535 tcp option maxseg size set 1324
    meta nfproto ipv4 tcp flags syn tcp option maxseg size 1345-65535 tcp option maxseg size set 1344
  '';

  # HACK: allow yggdrasil access from internal network
  #       this is not the stateless ipv6 prefix translation i would like it to be
  #       but it works???
  networking.nftables.extraConfig = ''
    table ip6 nat {
      chain postrouting {
        type nat hook postrouting priority 100
        iifname enp3s0 oifname yggdrasil snat ip6 to 303:dddd:fad8:d068:1::/80
      }
    }
    table ip nat {
      chain postrouting {
        type nat hook postrouting priority 100
        iifname { privnet, manglement } oifname "wgmesh-*" snat to 195.39.247.81
      }
    }
  '';

  services.avahi = {
    enable = true;
    openFirewall = true;
    reflector = false;

    nssmdns = true;
  };
}
