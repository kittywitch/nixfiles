{ config, lib, pkgs, profiles, users, ... }:

{
  imports = [
    ./hardware.nix
    profiles.desktop
    users.hexchen.desktopFull
    profiles.nopersist
  ];

  hexchen.deploy.groups = [ "desktop" ];

  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.supportedFilesystems = [ "nfs" ];

  networking.hostId = "814eeea0";

  users.users.hexchen.hashedPassword = "$6$BmZzI0sbTrN0c7D$FgJ5If1UIxlnfA3Ll1YVNhTS4US68zyoFtrGSKDbHaMn/NqXsv3TeINymQYp8GqN0RjrI7Pocpf1vBbz5kDD4/";
  users.users.root.hashedPassword = "$6$Amb39oDv$dOZKz81NaLEpgsfk1xmYQsQJMRjVN95mFfp.0KO/hx9sBxvw.3jWFCZUsltoCn2bNqz.CgjBAdz57ThCo9SFx1";
}
