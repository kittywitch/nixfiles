{ profiles, ... }:

{
  imports = [
    profiles.base
  ];
  hexchen.deploy.enable = false;

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    pubkey = "b5a16ff4826d31a76d44d546c0119b9e36f8fa85bb2224a4fc81c061294f7c45";
  };
}
