{ config, lib, pkgs, sources, modules, overlays, ... }:

with lib;

{
  imports = [
    modules.dns
    modules.network.yggdrasil
  ];
  nixpkgs.overlays = [ overlays.dns ];

  hexchen.deploy.enable = false;

  network.yggdrasil = {
    enable = true;
    pubkey = "0000000000000000000000000000000000000000000000000000000000000000";
    listen.enable = true;
    listen.endpoints = [];
    extra.pubkeys = {
      satorin = "53d99a74a648ff7bd5bc9ba68ef4f472fb4fb8b2e26dfecea33c781f0d5c9525";
      shanghai = "0cc3c26366cbfddfb1534b25c5655733d8f429edc941bcce674c46566fc87027";

      samhain = "a7110d0a1dc9ec963d6eb37bb6922838b8088b53932eae727a9136482ce45d47";
      yule = "9779fd6b5bdba6b9e0f53c96e141f4b11ce5ef749d1b9e77a759a3fdbd33a653";
      athame = "55e3f29c252d16e73ac849a6039824f94df1dee670c030b9e29f90584f935575";
      grimoire = "2a1567a2848540070328c9e938c58d40f2b1a3f08982c15c7edc5dcabfde3330";
      shit = "89684441745467da0d1bf7f47dc74ec3ca65e05c72f752298ef3c22a22024d43";
    };
  };
}
