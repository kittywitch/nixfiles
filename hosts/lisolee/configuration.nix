{ config, lib, pkgs, profiles, users, ... }:

{
  imports = [
    profiles.desktop
    users.hexchen.desktopFull
    ./hardware-configuration.nix
    ../../services/syncthing.nix
  ];

  hexchen.deploy.groups = [ "desktop" ];
  networking.domain = "net.lilwit.ch";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "nfs" ];
  networking.hostId = "007f0200";
  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
    extraModules = [ pkgs.pulseaudio-modules-bt ];
  };
  powerManagement.cpuFreqGovernor = "schedutil";

  programs.steam.enable = true;
  users.users.hexchen.packages = with pkgs; [ calibre blender ];

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    pubkey = "e0b010491d858dc5344e9e2cd54c38e44b418b01b3a0af1502678366780c5925";
  };

  system.stateVersion = "20.03";
}
