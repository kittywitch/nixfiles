{ config, pkgs, users, profiles, modules, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist
    ../../services/zfs.nix
    modules.nftnat
  ];

  hexchen.deploy.groups = [ "desktop" ];
  networking.domain = "net.lilwit.ch";

  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.supportedFilesystems = [ "nfs" ];
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  networking.hostId = "123318d1";
  networking.wireless.interfaces = [ "wlp3s0" ];

  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
    extraModules = [ pkgs.pulseaudio-modules-bt ];
    zeroconf.discovery.enable = true;
    tcp.enable = true;
  };
  powerManagement.cpuFreqGovernor = "schedutil";

  programs.steam.enable = true;
  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;
  services.fwupd.enable = true;
  users.users.hexchen.packages = with pkgs; [ 
    calibre multimc blender lutris 
    (wrapOBS { plugins = with obs-studio-plugins; [ wlrobs obs-move-transition ]; })
  ];

  users.users.hexchen.extraGroups = [ "wireshark" ];
  users.users.hexchen.hashedPassword = "$6$GHhUjotgJ/D$j77ekxK9ep9Ggxo9K3ROCaoDrMeeyrKUrCbsZIW.cRJAYMGCCDRRt4cqIUgzyjoq4d5gM0lDXdYseg5YkYM2Y.";
  users.users.root.hashedPassword = "$6$K9rb9VDrrt$3NcwGwSeZ560z1wQ.0vj6vB2aOpuUnJQ89TWf6nOHck.9Nluc6OEO4dLj8uRXSx3mNCHNMcwXYz2vR9uW7mpQ.";

  home-manager.users.hexchen = {
    home.sessionVariables = {
      GST_VAAPI_ALL_DRIVERS = 1;
    };
  };

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    paths = [
      "/home"
      "/persist"
    ];
    pruneOpts = [
      "--keep-daily 7"
      "--keep-weekly 5"
      "--keep-monthly 12"
    ];
    repository = "";
  };
  systemd.services."restic-backups-tardis".environment.RESTIC_REPOSITORY_FILE = "/persist/restic/system.repo";

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    pubkey = "e857573ca13e3082c2951a2b19c03b178703121f14f1d9ca4ceb581827933501";
  };

  networking.bridges.lxcbr0.interfaces = [];
  networking.interfaces.lxcbr0.ipv4.addresses = [
    {
      address = "10.53.118.1";
      prefixLength = 24;
    }
  ];

  networking.nftables.extraConfig = ''
    table ip nat {
      chain postrouting {
        type nat hook postrouting priority 100
        iifname lxcbr0 masquerade
      }
    }
  '';
  networking.nftables.forwardPolicy = "drop";
  networking.nftables.extraForward = ''
    iifname lxcbr0 accept
    oifname lxcbr0 accept
  '';
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;

  virtualisation.lxc.enable = true;
  virtualisation.lxc.systemConfig = ''
    lxc.bdev.zfs.root = zroot/lxc
    lxc.lxcpath = /persist/lxc
  '';
  virtualisation.lxc.usernetConfig = ''
    hexchen veth lxcbr0 10
  '';
  environment.etc."lxc/share".source = "${pkgs.lxc}/share/lxc";
  system.stateVersion = "20.09";
}
