{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "ehci_pci" "xhci_pci" "usb_storage" "usbhid" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = with config.boot.kernelPackages ; [ v4l2loopback ];
  boot.zfs.requestEncryptionCredentials = true;

  fileSystems."/" =
    { device = "zroot/nixos/root";
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "zroot/nixos/nix";
      fsType = "zfs";
    };

  fileSystems."/tmp" =
    { device = "zroot/tmp";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/47AA-59B0";
      fsType = "vfat";
    };

  fileSystems."/home" =
    { device = "zroot/vault/home";
      fsType = "zfs";
    };

  fileSystems."/persist" =
    { device = "zroot/vault/persist";
      fsType = "zfs";
    };

  swapDevices = [ ];

}
