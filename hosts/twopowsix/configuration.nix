{ config, lib, pkgs, profiles, users, modules, ... }:

{
  imports = [
    profiles.server
    users.hexchen.base
    modules.network.policyrouting
    ./hardware.nix
    ../../services/nat64.nix
  ];
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    zfs rollback -r rpool/local/root@blank
  '';
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "b8e08983";
  networking.hostName = lib.mkForce "2pow6";

  networking.useDHCP = true;

  networking.interfaces.ens3.ipv6.addresses = [ {
    address = "2a01:4f8:1c1c:7369::1";
    prefixLength = 64;
  } ];
  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "ens3";
  };
  hexchen.dns.zones."chaoswit.ch".subdomains."2pow6" = pkgs.dns.combinators.host "168.119.233.122" "2a01:4f8:1c1c:7369::1";

  services.openssh = {
    hostKeys = [
      {
        path = "/persist/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  networking.nameservers = [ "127.0.0.1" "1.1.1.1" ];
  networking.firewall.allowedUDPPorts = [ 51820 ];
  networking.wireguard.interfaces.cornbox = {
    privateKeyFile = "/persist/etc/wireguard/cornbox.key";
    ips = [ "195.39.247.71/32" "2a0f:4ac0:1337:64::1/128" ];
    listenPort = 51820;
    table = "51820";
    postSetup = "wg set cornbox fwmark 51820";
    peers = [
      {
        allowedIPs = [ "::/0" "0.0.0.0/0" ];
        publicKey = "8IWyiQL3wKP9CD/4UdS9b8mcbL67mkUyeSPORgEPvV0=";
        endpoint = "cornbox.hetzner.chaoswit.ch:51821";
      }
    ];
  };

  networking.policyrouting = {
    enable = true;
    rules4 = [
      { rule = "from 168.119.233.122/32 lookup main"; prio = 7000; }
      { rule = "lookup main suppress_prefixlength 0"; prio = 8000; }
      { rule = "not from all fwmark 51820 lookup 51820"; prio = 9000; }
    ];
    rules6 = [
      { rule = "from 2a0f:4ac0:1337:64::1/96 lookup 51820"; prio = 7000; }
    ];
  };
  networking.nftables.enable = false;
}
