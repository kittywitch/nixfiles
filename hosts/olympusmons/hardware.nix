{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ata_piix" "ahci" "firewire_ohci" "usb_storage" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/7276f4bb-539a-4839-b369-1f5f19476dd9";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/f5c9f6cc-4ee3-4c04-be4e-a290a72b5066";
      fsType = "ext2";
    };

  fileSystems."/mnt/sr0" =
    { device = "/dev/sr0";
      options = [ "ro" "user" "noauto" "unhide" ];
    };

  fileSystems."/rips" =
    { device = "storah.chaoswit.ch:/data/rips";
      fsType = "nfs";
      options = [ "x-systemd.automount" "noauto" ];
    };

  swapDevices = [
    {
      device = "/swapfile";
    }
  ];

  hardware.enableRedistributableFirmware = true;
}
